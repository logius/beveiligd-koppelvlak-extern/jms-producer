FROM registry.gitlab.com/logius/beveiligd-koppelvlak-extern/base-images/eclipse-temurin:17-jre-alpine-opentelemetry

USER root

# Create user to comply to numeric UID for k8s. This workaround should be adopted in the base image.
RUN addgroup producer && adduser -S -h /home/producer -G producer -u 1001 producer && \
  chown -R producer:producer /opt/app && \
  chown -h producer:producer /home/producer

USER 1001

COPY --chown=producer:producer target/*.jar /opt/app/app.jar

EXPOSE 9999
ENTRYPOINT ["java","-javaagent:opentelemetry-javaagent.jar","-XX:MaxRAMPercentage=70","-jar","/opt/app/app.jar"]
