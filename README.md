# JMS Producer & OSDBK

The JMS Producer is part of OSDBK (Open Source Dienst Beveiligd Koppelvlak). OSDBK consists of seven applications:

- EbMS Core / EbMS Admin
- JMS Producer
- JMS Consumer
- Apache ActiveMQ
- CPA Service
- OSDBK Admin Console
- Throttling Service (optional)

The JMS Producer takes care of handling messages between EbMS Core en ActiveMQ. For each received message, EbMS Core will send an event to the RECEIVED queue. The JMS Producer is listening to this queue and will pick up the event. The message body of the event holds the messageId of the received message. The JMS Producer calls the REST Message Service of EbMS Core and fetches the corresponding message and its attachment. It then routes the message to a pre-configured queue on ActiveMQ.


### Queue Routing Configuration
Incoming messages can be routed to a specific queue, based on OIN, CPA-ID, Service and/or Action of the message. In de `application.yml` file, routes can be added as follows:

~~~
jms-producer:
  configuration:
    routes:
      -
        oin: "*"
        cpaId: "*"
        service: "*"
        action: "verstrekkingDoorLV"
        queueName: "verstrekkingenInQueue"
      -
        oin: "*"
        cpaId: "*"
        service: "*"
        action: "afnemersberichtAanDGL"
        queueName: "example_queue_a"

~~~

The qualifier of each parameter can be replaced by a wildcard “*”. Any value retrieved from the message will be accepted for the route. The selection mechanism will route firstly on the most specifically declared parameter. A specific value will hence always overrule a wildcard.


### EbMS Core REST Service Configuration
EbMS Core exposes its REST Services by default on port 8888. If needed, the endpoints can be configured differently:
~~~
ebms:  
    message-service:  
        url: "http://ebms-core:8888/service/rest/v19/ebms/messages"  
~~~


### EbMS Core Event Queue Configuration
For each received message, EbMS Core will send an event to the RECEIVED queue. The JMS Producer is listening to this queue and will pick up the event. The queue-name can be configured in EbMS Core (default name is RECEIVED). The JMS Producer can adjust its queue listener configuration accordingly.
~~~
event:
  queue:
    received:
      name: RECEIVED
      concurrency: "2-4"
~~~

### ActiveMQ Configuration
The JMS Consumer implements the Spring Boot preconfigured ActiveMQ Listener with XA-transactions enabled. URL and credentials can be configured in the application.yml.

~~~
spring:
  activemq:
    broker-url: tcp://activemq:61616
    user: jmsconsumer
    password: jmsconsumer
~~~

### How to run this application locally
An example docker-compose has been provided. This can be run to test this application.
However, the following variables need to be populated by means of a `.env` file:
~~~
${OSDBK_IMAGE_REPOSITORY} -- location of your Docker repository where you are housing your built OSDBK docker images
${ACTIVEMQ_IMAGE_TAG} -- the tag of your built ActiveMQ docker image
${ACTIVEMQ_POSTGRESQL_USERNAME} -- the username used to connect to the ActiveMQ database
${ACTIVEMQ_POSTGRESQL_PASSWORD} -- the password used to connect to the ActiveMQ database
${DGL_STUB_IMAGE_TAG}  -- the tag of your built stub application docker image, to simulate interactions with an external party
${DGL_STUB_MESSAGE_SENDER_OIN_AFNEMER} -- the OIN of your stubbed application
${EBMS_CORE_IMAGE_TAG} -- the tag of your built ebms-core docker image
${EBMS_JDBC_USERNAME} -- the username used to connect to the EBMS database
${EBMS_JDBC_PASSWORD} -- the password used to connect to the EBMS database
${EBMS_JMS_BROKER_USERNAME} -- the username used to connect the ebms application to the ActiveMQ instance
${EBMS_JMS_BROKER_PASSWORD} -- the password used to connect the ebms application to the ActiveMQ instance
${EBMS_POSTGRESQL_IMAGE_TAG} -- the tag of your built EBMS PostgresQL docker image
${EBMS_STUB_IMAGE_TAG} -- the tag of your built stub application docker image, to simulate interactions with an external party
${JMS_CONSUMER_IMAGE_TAG} -- the tag of your built jms-consumer docker image
${JMS_CONSUMER_ACTIVEMQ_USER} -- the username used to connect the jms-consumer application to the ActiveMQ instance
${JMS_CONSUMER_ACTIVEMQ_PASSWORD} -- the password used to connect the jms-consumer application to the ActiveMQ instance
${JMS_PRODUCER_ACTIVEMQ_USER} -- the username used to connect the jms-producer application to the ActiveMQ instance
${JMS_PRODUCER_ACTIVEMQ_PASSWORD} -- the password used to connect the jms-producer application to the ActiveMQ instance
${THROTTLING_ENABLED} -- a boolean flag indicating whether throttling is enabled or disabled
${THROTTLING_URL} -- the endpoint of the throttling service to be used
${THROTTLING_SERVICE_IMAGE_TAG} -- the tag of your built throttling-service docker image
${CPA_SERVICE_IMAGE_TAG} -- the tag of your built cpa-service docker image
${CPA_JDBC_USERNAME} the username used to connect to the CPA database
${CPA_JDBC_PASSWORD} the password used to connect to the CPA database
${CPA_SERVICE_ACTIVEMQ_USER} -- the username used to connect the cpa-service application to the ActiveMQ instance
${CPA_SERVICE_ACTIVEMQ_PASSWORD} -- the password used to connect the cpa-service application to the ActiveMQ instance
${CPA_DATABASE_IMAGE_TAG} -- the tag of your built CPA PostgresQL docker image
${OSDBK_ADMIN_CONSOLE_IMAGE_TAG} -- the tag of your built OSDBK Admin console docker image
~~~

Example command to bring up the application:
~~~
docker-compose --env-file path/to/env/file/.env up
~~~

## Release notes
See [NOTES][NOTES] for latest.

[NOTES]: templates/NOTES.txt

### Older release notes collated below:

V Up to 1.12.3
- Various improvements to:
    - container technology
    - use of base-images
    - GITLAB Security scan framework implemented
    - Improved Open Source build process and container releases
    - Test improvements via docker-compose
    - Dependency upgrades
