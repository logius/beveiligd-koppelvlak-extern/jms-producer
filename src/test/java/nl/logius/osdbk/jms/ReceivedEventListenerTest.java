package nl.logius.osdbk.jms;

import jakarta.jms.JMSException;
import jakarta.jms.Message;
import nl.logius.osdbk.service.EbMSMessageService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReceivedEventListenerTest {

    @Mock
    private EbMSMessageService messageService;
    @Mock
    private JmsMessageProducer messageProducer;
    @InjectMocks
    private ReceivedEventListener eventListener;

    @Mock
    private Message message;

    @Mock
    private nl.clockwork.ebms.model.Message content;

    @BeforeEach
    void setup() throws JMSException {
        when(message.getStringProperty("messageId")).thenReturn("12345");
        when(messageService.getMessage("12345", true)).thenReturn(content);
    }

    @Test
    void onMessage() throws JMSException {
        eventListener.onMessage(message);
        verify(messageProducer, times(1)).sendMessage(content);
    }

    @Test
    void onMessageWithMessageNotFound() {
        when(messageService.getMessage("12345", true)).thenReturn(null);

        Assertions.assertThrows(JMSException.class, () -> {
            eventListener.onMessage(message);
        });
    }

}