package nl.logius.osdbk.jms.configuration;

import nl.logius.osdbk.exception.QueueNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class QueueServiceTest {
    private static final String DEFAULT_ACTION = "verstrekkingDoorLV";
    private static final String DEFAULT_QUEUE_NAME = "verstrekkingenInQueue";
    private static final String OIN = "12345";
    private static final String CPA_ID = "cpa-id";

    @Mock
    private JMSProducerConfiguration producerConfiguration;
    @InjectMocks
    private QueueService queueService;

    private QueueSearchCriteria searchCriteria;

    @Mock
    private JMSProducerConfiguration.Route route;

    @BeforeEach
    void setup() {
        searchCriteria = new QueueSearchCriteria(OIN, CPA_ID, "service", DEFAULT_ACTION);
        when(route.getOin()).thenReturn("*");
        when(route.getService()).thenReturn("*");
    }

    @Test
    void getQueueName() {
        when(route.getAction()).thenReturn(DEFAULT_ACTION);
        when(route.getCpaId()).thenReturn("*");
        when(route.getQueueName()).thenReturn(DEFAULT_QUEUE_NAME);
        when(producerConfiguration.getRoutes()).thenReturn(Arrays.asList(route));

        String queueName = queueService.getQueueName(searchCriteria);
        assertEquals(DEFAULT_QUEUE_NAME, queueName);
    }

    @Test
    void getQueueNameNotFound() {
        when(route.getCpaId()).thenReturn("*");
        when(route.getAction()).thenReturn("verstrekkingAanAfnemer");
        when(producerConfiguration.getRoutes()).thenReturn(Arrays.asList(route));

        Assertions.assertThrows(QueueNotFoundException.class, () -> {
            String queueName = queueService.getQueueName(searchCriteria);
        });
    }

    @Test
    void getQueueNameMostSpecific() {
        when(route.getAction()).thenReturn(DEFAULT_ACTION);
        when(route.getCpaId()).thenReturn("*");

        JMSProducerConfiguration.Route specificRoute = mock(JMSProducerConfiguration.Route.class);
        when(specificRoute.getOin()).thenReturn(OIN); // OIN is set, so more specific than default
        when(specificRoute.getCpaId()).thenReturn("*");
        when(specificRoute.getService()).thenReturn("*");
        when(specificRoute.getAction()).thenReturn(DEFAULT_ACTION);
        when(specificRoute.getQueueName()).thenReturn("specificOinQueue");

        when(producerConfiguration.getRoutes()).thenReturn(Arrays.asList(route, specificRoute));

        String queueName = queueService.getQueueName(searchCriteria);
        assertEquals("specificOinQueue", queueName);
    }

    @Test
    void getQueueNameExpectOinBeforeCpaMatch() {
        when(route.getAction()).thenReturn(DEFAULT_ACTION);
        when(route.getCpaId()).thenReturn(CPA_ID);

        JMSProducerConfiguration.Route specificRoute = mock(JMSProducerConfiguration.Route.class);
        when(specificRoute.getOin()).thenReturn(OIN);
        when(specificRoute.getCpaId()).thenReturn("*");
        when(specificRoute.getService()).thenReturn("*");
        when(specificRoute.getAction()).thenReturn(DEFAULT_ACTION);
        when(specificRoute.getQueueName()).thenReturn("specificOinQueue");

        when(producerConfiguration.getRoutes()).thenReturn(Arrays.asList(route, specificRoute));

        String queueName = queueService.getQueueName(searchCriteria);
        assertEquals("specificOinQueue", queueName); // first queue has CPA-ID, second has OIN. OIN comes first, so wins
    }

}