package nl.logius.osdbk.jms.factory;

import jakarta.activation.DataHandler;
import jakarta.jms.*;
import nl.clockwork.ebms.model.DataSource;
import nl.clockwork.ebms.model.MessageProperties;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.utility.DockerImageName;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import jakarta.jms.ConnectionFactory;
import jakarta.jms.JMSException;
import jakarta.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.ClassRule;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.utility.DockerImageName;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { MessageFactoryTest.ThisTestConfiguration.class })
public class MessageFactoryTest {

    @ClassRule
    public static GenericContainer<?> activeMqContainer = new GenericContainer<>(DockerImageName.parse("rmohr/activemq:5.14.3")).withExposedPorts(61616);

    @Autowired
    private JmsTemplate jmsTemplate;

    @InjectMocks
    private MessageFactory messageFactory;

    @Test
    public void createTextMessage() throws Exception {
        Session session = mock(Session.class);
        nl.clockwork.ebms.model.Message ebmsMessage = mock(nl.clockwork.ebms.model.Message.class);
        DataSource dataSource = mock(DataSource.class);
        MessageProperties messageProperties = mock(MessageProperties.class, Mockito.RETURNS_DEEP_STUBS);
        DataHandler attachment = mock(DataHandler.class);
        when(ebmsMessage.getDataSources()).thenReturn(Arrays.asList(dataSource));
        when(dataSource.getContent()).thenReturn("mock-ebmsMessage".getBytes());
        when(session.createTextMessage("mock-ebmsMessage")).thenReturn(createEmptyMessage());
        when(ebmsMessage.getProperties()).thenReturn(messageProperties);

        when(messageProperties.getAction()).thenReturn("verstrekkingDoorLV");
        when(messageProperties.getService()).thenReturn("dgl:ontvangen:1.0");
        when(messageProperties.getMessageId()).thenReturn("message-id");
        when(messageProperties.getToParty().getPartyId()).thenReturn("12345");
        when(messageProperties.getFromParty().getPartyId()).thenReturn("54321");
        when(messageProperties.getConversationId()).thenReturn("conversation-id");

        Message message = messageFactory.createTextMessage(session, ebmsMessage);
        assertEquals("message-id", message.getStringProperty("x_aux_system_msg_id"));
        assertEquals("verstrekkingDoorLV", message.getStringProperty("x_aux_action"));
        assertEquals("dgl:ontvangen:1.0", message.getStringProperty("x_aux_activity"));
        assertEquals("dgl:ontvangen:1.0", message.getStringProperty("x_aux_process_type"));
        message.setStringProperty("12345", message.getStringProperty("x_aux_sender_id"));
        message.setStringProperty("54321", message.getStringProperty("x_aux_receiver_id"));
        message.setStringProperty("conversation-id", message.getStringProperty("x_aux_process_instance_id"));
    }

    private TextMessage createEmptyMessage() throws JMSException {
        return jmsTemplate.getConnectionFactory().createConnection().createSession().createTextMessage();
    }

    @Configuration
    @EnableJms
    static class ThisTestConfiguration {
        @Bean
        public JmsListenerContainerFactory<?> jmsListenerContainerFactory() {
            DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
            factory.setConnectionFactory(connectionFactory());
            return factory;
        }

        @Bean
        public ConnectionFactory connectionFactory() {
            String brokerUrlFormat = "tcp://%s:%d";
            String brokerUrl = String.format(brokerUrlFormat, activeMqContainer.getHost(), activeMqContainer.getFirstMappedPort());
            return new ActiveMQConnectionFactory(brokerUrl);
        }

        @Bean
        public JmsTemplate jmsTemplate() {
            return new JmsTemplate(connectionFactory());
        }
    }

}