package nl.logius.osdbk.service;

import nl.clockwork.ebms.model.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

import static org.mockito.Mockito.*;

@SpringBootTest
class EbMSMessageServiceTest {

    @InjectMocks
    private EbMSMessageService service;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ResponseEntity<Message> responseMock;

    @Mock
    Message message;

    private final String url = "http://localhost:8888/service/rest/v19/ebms/messages";
    private final String username = "user";
    private final String password = "pw";

    @BeforeEach
    void setup() {
        ReflectionTestUtils.setField(service, "ebmsMessageServiceEndpoint", url);
        ReflectionTestUtils.setField(service, "ebmsMessageServiceUser", username);
        ReflectionTestUtils.setField(service, "ebmsMessageServicePassword", password);

        when(restTemplate.exchange(any(URI.class), any(HttpMethod.class), any(HttpEntity.class), any(Class.class))).thenReturn(responseMock);

    }

    @Test
    void getMessage() {

        Boolean process = true;
        String messageId = "performance_f9b9vmNPSBitvi8Q2i1uNg%2F0000000000";

        when(responseMock.getBody()).thenReturn(message);

        service.getMessage(messageId, process);

        verify(restTemplate, times(1)).exchange(any(URI.class), any(HttpMethod.class), any(HttpEntity.class), any(Class.class));

    }
}