package nl.logius.osdbk.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class EbMSBrokerUtilsTest {

    @Test
    void stripUrnPrefixes() {
        String service = EbMSBrokerUtils.stripUrnPrefixes("urn:osb:services:test");
        Assertions.assertEquals("test", service);
        String oin = EbMSBrokerUtils.stripUrnPrefixes("urn:osb:oin:test");
        Assertions.assertEquals("test", oin);
    }
}