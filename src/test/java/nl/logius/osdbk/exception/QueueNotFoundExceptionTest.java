package nl.logius.osdbk.exception;

import nl.logius.osdbk.exception.QueueNotFoundException;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class QueueNotFoundExceptionTest {

    @Test
    void queueNotFoundException(){

        Throwable exception = assertThrows(QueueNotFoundException.class, () -> {
            throw new QueueNotFoundException("test");
        }  );

        assertEquals("test", exception.getMessage());

    }
}