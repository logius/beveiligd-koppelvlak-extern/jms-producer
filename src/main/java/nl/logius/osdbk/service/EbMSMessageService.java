package nl.logius.osdbk.service;

import nl.clockwork.ebms.model.MessageRequest;
import nl.logius.osdbk.util.RestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import nl.clockwork.ebms.model.Message;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
public class EbMSMessageService {

    @Value("${ebms.message-service.url}")
    private String ebmsMessageServiceEndpoint;
    @Value("${ebms.message-service.username}")
    private String ebmsMessageServiceUser;
    @Value("${ebms.message-service.password}")
    private String ebmsMessageServicePassword;

    private final RestTemplate restTemplate;

    @Autowired
    public EbMSMessageService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Message getMessage(String messageId, Boolean process) {

        final Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("messageId", URLEncoder.encode(messageId, StandardCharsets.UTF_8));

        var path = UriComponentsBuilder.fromPath("/{messageId}")
                .buildAndExpand(uriVariables)
                .toUriString();

        var uri = UriComponentsBuilder.fromUriString(ebmsMessageServiceEndpoint)
                .path(path)
                .queryParam("process", String.valueOf(process))
                .build(true)
                .toUri();

        HttpHeaders headers = RestUtils.createHeadersWithBasicAuth(ebmsMessageServiceUser, ebmsMessageServicePassword);
        HttpEntity<MessageRequest> request = new HttpEntity<>(headers);
        ResponseEntity<Message> response = restTemplate.exchange(uri, HttpMethod.GET, request, Message.class);
        return response.getBody();
    }

}
