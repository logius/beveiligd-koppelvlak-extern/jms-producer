package nl.logius.osdbk.util;

import nl.clockwork.ebms.model.MessageProperties;

import java.util.HashMap;
import java.util.Map;

public class LoggingUtils {

    private LoggingUtils(){}

    public static Map<String, String> getPropertyMap(MessageProperties messageProperties, Map<String, String> additionalProperties){
        Map<String, String> properties = new HashMap<>(4, 1.0f);
        properties.put("messageId", messageProperties.getMessageId());
        properties.put("conversationId", messageProperties.getConversationId());
        properties.put("sender", EbMSBrokerUtils.stripUrnPrefixes(messageProperties.getFromParty().getPartyId()));
        properties.put("senderName", messageProperties.getFromParty().getRole());
        properties.put("receiver", EbMSBrokerUtils.stripUrnPrefixes(messageProperties.getToParty().getPartyId()));
        properties.put("receiverName", messageProperties.getToParty().getRole());
        properties.put("action", messageProperties.getAction());

        properties.putAll(additionalProperties);
        return properties;
    }
}
