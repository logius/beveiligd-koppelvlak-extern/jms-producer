package nl.logius.osdbk.util;

import org.apache.commons.lang3.StringUtils;

public class EbMSBrokerUtils {

    private EbMSBrokerUtils(){}

    private static final String OIN_PREFIX = "urn:osb:oin:";

    public static String stripUrnPrefixes(String text) {
        text = StringUtils.remove(text, "urn:osb:services:");
        text = StringUtils.remove(text, OIN_PREFIX);
        return text;
    }

}
