package nl.logius.osdbk.jms;

import nl.clockwork.ebms.model.Message;
import nl.clockwork.ebms.model.MessageProperties;
import nl.logius.osdbk.jms.configuration.QueueSearchCriteria;
import nl.logius.osdbk.jms.configuration.QueueService;
import nl.logius.osdbk.exception.EventException;
import nl.logius.osdbk.jms.factory.MessageFactory;
import nl.logius.osdbk.util.LoggingUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Component
public class JmsMessageProducer {
    private static final Logger LOGGER = LoggerFactory.getLogger(JmsMessageProducer.class);

    private final QueueService queueService;
    private final JmsTemplate jmsTemplate;
    private final MessageFactory messageFactory;

    @Autowired
    public JmsMessageProducer(QueueService queueService, JmsTemplate jmsTemplate, MessageFactory messageFactory) {
        this.queueService = queueService;
        this.jmsTemplate = jmsTemplate;
        this.messageFactory = messageFactory;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void sendMessage(Message message) {
        MessageProperties messageProperties = message.getProperties();
        QueueSearchCriteria searchCriteria = new QueueSearchCriteria(
                messageProperties.getFromParty().getPartyId(),
                messageProperties.getCpaId(),
                messageProperties.getService(),
                messageProperties.getAction());

        String queueName = queueService.getQueueName(searchCriteria);
        MDC.setContextMap(LoggingUtils.getPropertyMap(messageProperties, Map.of("queueName", queueName)));

        if (message.getDataSources() == null || message.getDataSources().isEmpty()) {
            throw new EventException("JMS001: Cannot process message " + messageProperties.getMessageId() +
                    " no message datasources!");
        }

        LOGGER.info("Queueing message");
        jmsTemplate.send(queueName, session -> messageFactory.createTextMessage(session, message));
        LOGGER.info("Message successfully sent to queue");
        MDC.clear();
    }
}