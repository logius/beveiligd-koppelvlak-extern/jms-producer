package nl.logius.osdbk.jms;

import jakarta.jms.JMSException;
import nl.clockwork.ebms.model.Message;
import nl.logius.osdbk.service.EbMSMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientResponseException;

@Component
@ConditionalOnProperty(value="jms-producer.enabled", havingValue = "true", matchIfMissing = true)
public class ReceivedEventListener {
    private final Logger logger = LoggerFactory.getLogger(ReceivedEventListener.class);

    private final EbMSMessageService messageService;
    private final JmsMessageProducer messageProducer;

    @Autowired
    public ReceivedEventListener(EbMSMessageService messageService, JmsMessageProducer messageProducer) {
        this.messageService = messageService;
        this.messageProducer = messageProducer;
    }

    @Retryable(
            value = ResourceAccessException.class,  // infinite retry when ebMS Core endpoint is unavailable
            maxAttempts = Integer.MAX_VALUE,
            backoff = @Backoff(delay = 1000, maxDelay = 30000),
            listeners = "jmsRetryListener"
    )
    @JmsListener(destination = "${event.queue.received.name}", concurrency = "${event.queue.received.concurrency}", containerFactory = "jmsListenerContainerFactory")
    @Transactional(propagation = Propagation.REQUIRED)
    public void onMessage(jakarta.jms.Message jmsMessage) throws RestClientResponseException, JMSException {
        String messageId = jmsMessage.getStringProperty("messageId");
        logger.debug(String.format("Received event for message %s", messageId));

        Message message = messageService.getMessage(messageId, true);
        if(message == null) {
            throw new JMSException(String.format("Message with ID %s does not exists.", messageId));
        }

        messageProducer.sendMessage(message);
    }
}