package nl.logius.osdbk.jms.configuration;

import nl.logius.osdbk.exception.QueueNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class QueueService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(QueueService.class);

    @Autowired
    private JMSProducerConfiguration configuration;

    public String getQueueName(QueueSearchCriteria searchCriteria) {
        LOGGER.debug(searchCriteria.toString());
        List<JMSProducerConfiguration.Route> matchingRoutes = configuration.getRoutes().stream()
                .peek(route -> LOGGER.debug(route.toString()))
                .filter(route -> route.getOin().equals(searchCriteria.getOin()) || isWildcard(route.getOin()))
                .filter(route -> route.getCpaId().equals(searchCriteria.getCpaId()) || isWildcard(route.getCpaId()))
                .filter(route -> route.getService().equals(searchCriteria.getService()) || isWildcard(route.getService()))
                .filter(route -> route.getAction().equals(searchCriteria.getAction()) || isWildcard(route.getAction()))
                .collect(Collectors.toList());

        JMSProducerConfiguration.Route route = findMostSpecificRoute(matchingRoutes);
        return route.getQueueName();
    }

    private JMSProducerConfiguration.Route findMostSpecificRoute(List<JMSProducerConfiguration.Route> routes) {
        Collections.sort(routes, Comparator.comparing(JMSProducerConfiguration.Route::getOin, this::compare)
                .thenComparing(JMSProducerConfiguration.Route::getCpaId, this::compare)
                .thenComparing(JMSProducerConfiguration.Route::getService, this::compare)
                .thenComparing(JMSProducerConfiguration.Route::getAction, this::compare));

        return routes.stream().findFirst()
                .orElseThrow(() -> new QueueNotFoundException("No queue configuration found."));
    }

    private boolean isWildcard(String match) {
        return "*".equals(match);
    }

    private int compare(String input, String match) {
        int value = input.equals(match) ? -1 : 0; // if equal, always comes first, in ordered sorting smaller is better
        return value + (!isWildcard(input) && isWildcard(match) ? -1 : 0); // if first input is not a wildcard and second is, is should be ordered first
    }
}
