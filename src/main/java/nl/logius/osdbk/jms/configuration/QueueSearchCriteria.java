package nl.logius.osdbk.jms.configuration;

public class QueueSearchCriteria {
    private final String oin;
    private final String cpaId;
    private final String service;
    private final String action;

    public QueueSearchCriteria(String oin, String cpaId, String service, String action) {
        this.oin = oin;
        this.cpaId = cpaId;
        this.service = service;
        this.action = action;
    }

    public String getOin() {
        return oin;
    }

    public String getCpaId() {
        return cpaId;
    }

    public String getService() {
        return service;
    }

    public String getAction() {
        return action;
    }

    @Override
    public String toString() {
        return "QueueSearchCriteria{" + "oin=" + oin + ", cpaId=" + cpaId + ", service=" + service + ", action=" + action + '}';
    }
    
}
