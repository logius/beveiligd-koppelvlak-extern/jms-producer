package nl.logius.osdbk.jms.factory;

import jakarta.jms.JMSException;
import jakarta.jms.Session;
import jakarta.jms.TextMessage;
import nl.clockwork.ebms.model.DataSource;
import nl.clockwork.ebms.model.MessageProperties;
import nl.logius.osdbk.util.EbMSBrokerUtils;
import org.springframework.stereotype.Component;

@Component
public class MessageFactory {

    public TextMessage createTextMessage(Session session, nl.clockwork.ebms.model.Message message) throws JMSException {
        MessageProperties messageContext = message.getProperties();
        DataSource datasource = message.getDataSources().get(0);

        TextMessage textMessage = session.createTextMessage(new String(datasource.getContent()));
        setJMSHeaders(textMessage, messageContext);

        return textMessage;
    }

     private void setJMSHeaders(TextMessage message, MessageProperties messageProperties) throws JMSException {
        message.setStringProperty("x_aux_action", messageProperties.getAction());
        message.setStringProperty("x_aux_activity", EbMSBrokerUtils.stripUrnPrefixes(messageProperties.getService()));
        message.setStringProperty("x_aux_process_type", EbMSBrokerUtils.stripUrnPrefixes(messageProperties.getService()));
        message.setStringProperty("x_aux_process_version", "1.0");
        message.setStringProperty("x_aux_production", "Production");
        message.setStringProperty("x_aux_protocol", "ebMS");
        message.setStringProperty("x_aux_protocol_version", "2.0");
        message.setStringProperty("x_aux_system_msg_id", messageProperties.getMessageId());
        message.setStringProperty("x_aux_receiver_id", EbMSBrokerUtils.stripUrnPrefixes(messageProperties.getToParty().getPartyId()));
        message.setStringProperty("x_aux_sender_id", EbMSBrokerUtils.stripUrnPrefixes(messageProperties.getFromParty().getPartyId()));
        message.setStringProperty("x_aux_process_instance_id", messageProperties.getConversationId());
        message.setStringProperty("x_aux_seq_number", "0");
        message.setStringProperty("x_aux_msg_order", "false");
    }

}
