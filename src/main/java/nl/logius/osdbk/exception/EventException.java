package nl.logius.osdbk.exception;

public class EventException extends RuntimeException {

    public EventException(String message) {
        super(message);
    }

}
